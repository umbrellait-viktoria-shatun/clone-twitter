import React, { FC } from 'react';
import { Paper, Typography } from '@material-ui/core';
import { AddTweetForm } from '../components/AddTweetForm';
import { useHomeStyles } from './theme';

interface BookmarksProps {
  classes: ReturnType<typeof useHomeStyles>;
}

export const Bookmarks: FC<BookmarksProps> = ({classes}) => {
  return (
    <div>
      <Paper className={classes.tweetsWrapper} variant="outlined">
        <Paper className={classes.tweetsHeader} variant="outlined">
          <Typography variant="h6">Обзор</Typography>
        </Paper>
        <Paper>
          <div className={classes.addForm}>
            <AddTweetForm classes={classes}/>
          </div>
          <div className={classes.addFormBottomLine}/>
        </Paper>
      </Paper>
    </div>

  );
};