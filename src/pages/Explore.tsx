import React, { FC } from 'react';
import { Paper } from '@material-ui/core';
import { AddTweetForm } from '../components/AddTweetForm';
import { useHomeStyles } from './theme';
import { SearchField } from '../components/SearchField';
import { BackButton } from '../components/BackButton';

interface ExploreProps {
  classes: ReturnType<typeof useHomeStyles>;
}

export const Explore: FC<ExploreProps> = ({classes}) => {
  return (
    <div>
      <Paper className={classes.tweetsWrapper} variant="outlined">
        <Paper className={classes.tweetsHeader} variant="outlined">
          <BackButton/> <SearchField/>
        </Paper>
        <Paper>
          <div className={classes.addForm}>
            <AddTweetForm classes={classes}/>
          </div>
          <div className={classes.addFormBottomLine}/>
        </Paper>
      </Paper>
    </div>

  );
};