import React, { FC, useState } from "react";
import { bigTwitter } from "../../icon";
import "./SignIn.scss";
import { LoginModal } from "./components/LoginModal";
import { RegisterModal } from "./components/RegisterModal";

import TwitterIcon from "@material-ui/icons/Twitter";
import { makeStyles, Typography } from "@material-ui/core";

export const useStylesSignIn = makeStyles((theme) => ({
  loginSideField: {
    marginBottom: 18,
  },
  loginSideButton: {
    borderRadius: 20,
    color: "#fff",
    background: "rgb(29, 161, 242)",
    "&:hover": {
      background: "rgb(26, 145, 218)",
    },
  },
  registerField: {
    marginBottom: theme.spacing(5),
  },
  registerButton: {
    borderRadius: 20,
    color: "#fff",
    background: "rgb(29, 161, 242)",
    "&:hover": {
      background: "rgb(26, 145, 218)",
    },
  },
  loginFormControl: {
    marginBottom: theme.spacing(2),
  },
}));

export const SignIn: FC = () => {
  const [visibleModal, setVisibleModal] = useState<"signIn" | "signUp">();

  const handleClickOpenSignIn = () => {
    setVisibleModal("signIn");
  };

  const handleClickOpenSignUp = () => {
    setVisibleModal("signUp");
  };

  const handleCloseModal = () => {
    setVisibleModal(undefined);
  };
  return (
    <div className="wrapper">
      <section className="leftSide">
        <div className="leftTwitter">{bigTwitter}</div>
      </section>
      <section className="rightSide">
        <div className="singInBlock">
          <div className="description">
            <TwitterIcon fontSize="large" className="singInIcon" />
            <Typography variant="h2" className="descriptionBigText">
              В курсе происходящего
            </Typography>
            <Typography variant="h5" className="descriptionSmallText">
              Присоединяйтесь к Твиттеру прямо сейчас!
            </Typography>
          </div>

          <div className="buttonBlock">
            <button onClick={handleClickOpenSignUp} className="register">
              Зарегистрироваться
            </button>
            <button onClick={handleClickOpenSignIn} className="login">
              Войти
            </button>
          </div>
        </div>
      </section>

      <LoginModal open={visibleModal === "signIn"} onClose={handleCloseModal} />
      <RegisterModal
        open={visibleModal === "signUp"}
        onClose={handleCloseModal}
      />
    </div>
  );
};
