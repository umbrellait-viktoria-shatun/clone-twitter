import React, { FC, ReactElement, ReactNode} from "react";
import { Container, Grid } from "@material-ui/core";
import { useHomeStyles } from "./theme";
import { LeftPanel } from "../components/leftPanel";
import { RightPanel } from "../components/rightPanel";

interface LayoutProps {
  children: ReactNode;
}

export const Layout: FC<LayoutProps> = ({ children }): ReactElement => {
  const classes = useHomeStyles();

  return (
    <Container className={classes.wrapper} maxWidth="xl">
      <Grid container spacing={3} style={{ width: "100%", margin: 0 }}>
        <Grid sm={2} md={3} item style={{ padding: 0 }}>
          <LeftPanel classes={classes} />
        </Grid>
        <Grid sm={7} md={6} item style={{ padding: 0 }}>
          {children}
        </Grid>
        <Grid sm={3} md={3} item style={{ padding: 0 }}>
          <RightPanel classes={classes} />
        </Grid>
      </Grid>
    </Container>
  );
};
