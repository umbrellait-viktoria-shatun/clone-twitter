import { colors, makeStyles, Theme } from '@material-ui/core';
import grey from '@material-ui/core/colors/grey';

export const useHomeStyles = makeStyles((theme: Theme) => ({
  centered: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
  },
  wrapper: {
    padding: '0'
  },
  logo: {
    margin: '10px 0'
  },
  logoIcon: {
    fontSize: 36
  },
  tweetsWrapper: {
    borderRadius: 0,
    minHeight: '100vh',
    borderTop: '0',
    borderBottom: '0',
  },
  tweetsCentred: {
    marginTop: 50,
    textAlign: 'center',
  },
  tweetsHeader: {
    display: 'flex',
    alignItems: 'center',
    flex: 1,
    borderTop: '0',
    borderLeft: '0',
    borderRight: '0',
    borderRadius: 0,
    padding: '10px 15px',
    '& h6': {
      fontWeight: 800,
    },
  },
  tweetsHeaderUser: {
    display: 'flex',
    alignItems: 'center',
  },
  tweetsHeaderBackButton: {
    marginRight: 20,
  },
  tweet: {
    display: 'flex',
    cursor: 'pointer',
    alignItems: 'flex-start',
    paddingTop: 15,
    paddingLeft: 20,
    '&:hover': {
      backgroundColor: 'rgb(245, 248, 250)',
    },
  },
  tweetWrapper: {
    color: 'inherit',
    textDecoration: 'none',
  },
  tweetAvatar: {
    width: theme.spacing(6.5),
    height: theme.spacing(6.5),
    marginRight: 15,
  },
  tweetHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  tweetContent: {
    flex: 1,
  },
  tweetFooter: {
    display: 'flex',
    position: 'relative',
    left: -13,
    justifyContent: 'space-between',
    maxWidth: 450,
  },
  tweetUserName: {
    color: grey[500],
  },
  fullTweet: {
    padding: 22,
    paddingBottom: 0,
  },
  fullTweetText: {
    fontSize: 24,
    marginTop: 20,
    marginBottom: 20,
    lineHeight: 1.3125,
    wordBreak: 'break-word',
  },
  fullTweetFooter: {
    margin: '0 auto',
    borderTop: '1px solid #E6ECF0',
    left: 0,
    maxWidth: '100%',
    justifyContent: 'space-around',
    padding: '2px 0',
    marginTop: 20,
  },


  rightSide: {
    background: '#15202b',
    color: '#fff',
    padding: '12px 20px 0',
    position: 'sticky',
    top: 0,
    height: '100vh'
  },
  rightSideBlock: {
    backgroundColor: '#F5F8FA',
    borderRadius: 15,
    marginTop: 20,
    '& .MuiList-root': {
      paddingTop: 0
    }
  },
  rightSideBlockHeader: {
    borderTop: 0,
    borderLeft: 0,
    borderRight: 0,
    backgroundColor: 'transparent',
    padding: '13px 18px',
    '& b': {
      fontSize: 20,
      fontWeight: 800
    }
  },
  rightSideBlockItem: {
    cursor: 'pointer',
    '& .MuiTypography-body1': {
      fontWeight: 700
    },
    '& .MuiListItemAvatar-root': {
      minWidth: 50
    },
    '& .MuiListItemText-root': {
      margin: 0
    },
    '&:hover': {
      backgroundColor: '#edf3f6'
    },
    '& a': {
      color: 'inherit',
      textDecoration: 'none'
    }

  },
  addForm: {
    padding: 20
  },
  addFormBody: {
    display: 'flex',
    width: '100%'
  },
  addFormBottom: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  addFormBottomActions: {
    marginTop: 10,
    paddingLeft: 70
  },
  addFormTextarea: {
    width: '100%',
    border: 0,
    fontSize: 20,
    outline: 'none',
    fontFamily: 'inherit',
    resize: 'none'
  },
  addFormBottomLine: {
    height: 12,
    backgroundColor: '#E6ECF0'
  },
  addFormCircleProgress: {
    position: 'relative',
    width: 20,
    height: 20,
    margin: '0 10px',
    '& .MuiCircularProgress-root': {
      position: 'absolute'
    }
  },
  addFormBottomRight: {
    display: 'flex',
    alignItems: 'center'
  },
  sideProfile: {
    display: 'flex',
    alignItems: 'center',
    position: 'fixed',
    bottom: 30,
    padding: '10px 15px',
    // width: 210,
    width: 'fit-content',
    borderRadius: 50,
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: colors.blue[400],
    },
  },
  sideProfileInfo: {
    flex: 1,
    marginLeft: 10,
    '& b': {
      fontSize: 16,
    },
  },
  imagesList: {
    display: 'flex',
    alignItems: 'center',
    marginTop: 20,
    flexWrap: 'wrap',
  },
  imagesListItem: {
    marginRight: 10,
    marginBottom: 10,
    position: 'relative',
    '& img': {
      width: '100%',
      height: '100%',
      'object-fit': 'cover',
      borderRadius: 6,
    },
    '& svg path': {
      fill: 'white',
    },
  },
  profileMenu: {
    top: 'auto !important',
    left: '17.5% !important',
    width: '250px !important',
    bottom: '110px !important',
    'box-shadow': '1px 1px 10px rgba(0, 0, 0, 0.08)',
    'border-radius': '20px',
    border: '1px solid rgba(0, 0, 0, 0.1)',
    '& a': {
      color: 'black',
      textDecoration: 'none',
    },
  },
  imagesListItemRemove: {
    position: 'absolute',
    top: -8,
    right: -6,
    padding: '0 !important',
    backgroundColor: '#ff4d4d !important',
  },
}));
