import React, { FC } from "react";
import {
  InputAdornment,
  TextField,
  Theme,
  withStyles,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/SearchOutlined";

export const SearchTextField = withStyles((theme: Theme) => ({
  root: {
    "& .MuiOutlinedInput-root": {
      borderRadius: 30,
      backgroundColor: "#E6ECF0",
      padding: 0,
      paddingLeft: 15,
      "&.Mui-focused": {
        backgroundColor: "#fff",
        "& fieldset": {
          borderWidth: 1,
          borderColor: theme.palette.primary.main,
        },
        "& svg path": {
          fill: theme.palette.primary.main,
        },
      },
      "&:hover": {
        "& fieldset": { borderColor: "transparent" },
      },
      "& fieldset": {
        borderColor: "transparent",
        borderWidth: 1,
      },
    },
    "& .MuiOutlinedInput-input": {
      padding: "12px 14px 14px 5px",
    },
  },
}))(TextField);

interface SearchProps {}

export const SearchField: FC<SearchProps> = () => {
  return (
    <SearchTextField
      variant="outlined"
      placeholder="Поиск по Твиттеру"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        ),
      }}
      fullWidth
    />
  );
};
