import React, { FC, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Hidden } from '@material-ui/core';
import CreateIcon from '@material-ui/icons/Create';
import './leftPanel.scss';
import { bookmarks, explore, home, lists, messages, more, notification, profile, twitter } from '../icon';
import { useHomeStyles } from '../pages/theme';
import { AddTweetForm } from './AddTweetForm';
import { ModalBlock } from './ModalBlock';
import { UserSideProfile } from './UserSideProfile';
import { useSelector } from 'react-redux';
import { selectUserData } from '../store/ducks/user/selectors';


interface LeftPanelProps {
  classes: ReturnType<typeof useHomeStyles>;
}

export const LeftPanel: FC<LeftPanelProps> = ({ classes }) => {
  const [visibleAddTweet, setSetVisibleAddTweet] = useState<boolean>(false);

  const userData = useSelector(selectUserData);

  const handleClickOpenAddTweet = () => {
    setSetVisibleAddTweet(true);
  };

  const onCloseAddTweet = () => {
    setSetVisibleAddTweet(false);
  };

  return (
    <div className="left-panel">
      <div className="container">
        <Link to="/home">
          <header>{twitter}</header>
        </Link>
        <nav>
          <NavLink to="/home" exact activeClassName="selected">
            <span>
              {home}
              <Hidden mdDown>
                <div className="menu">Главная</div>
              </Hidden>
            </span>
          </NavLink>
          <NavLink to="/explore" activeClassName="selected">
            <span>
              {explore}
              <Hidden mdDown>
                <div className="menu">Обзор</div>
              </Hidden>
            </span>
          </NavLink>
          <NavLink to="/notification" activeClassName="selected">
            <span>
              {notification}
              <Hidden mdDown>
                <div className="menu">Уведомления</div>
              </Hidden>
            </span>
          </NavLink>
          <NavLink to="/messages" activeClassName="selected">
            <span>
              {messages}
              <Hidden mdDown>
                <div className="menu">Сообщения</div>
              </Hidden>
            </span>
          </NavLink>
          <NavLink to="/bookmarks" activeClassName="selected">
            <span>
              {bookmarks}
              <Hidden mdDown>
                <div className="menu">Закладки</div>
              </Hidden>{" "}
            </span>
          </NavLink>
          <NavLink to="/lists" activeClassName="selected">
            <span>
              {lists}
              <Hidden mdDown>
                <div className="menu">Списки</div>
              </Hidden>
            </span>
          </NavLink>
          <NavLink to={`/user/${userData?._id}`} activeClassName="selected">
            <span>
              {profile}
              <Hidden mdDown>
                <div className="menu">Профиль</div>
              </Hidden>
            </span>
          </NavLink>
          <button className="more">
            <span>
              {more}
              <Hidden mdDown>
                <div className="menu">Еще</div>
              </Hidden>
            </span>
          </button>
        </nav>
        <Hidden mdDown>
          <button className="tweet" onClick={handleClickOpenAddTweet}>
            Твитнуть
          </button>
        </Hidden>
        <Hidden lgUp>
          <button className="tweetMini" onClick={handleClickOpenAddTweet}>
            <CreateIcon />
          </button>
        </Hidden>
        <div>
          <ModalBlock onClose={onCloseAddTweet} visible={visibleAddTweet}>
            <div style={{ width: 550 }}>
              <AddTweetForm maxRows={15} classes={classes} />
            </div>
          </ModalBlock>
        </div>
        <footer>
          <UserSideProfile classes={classes} />
          {/*<button className="account">*/}
          {/*  <div className="photo">*/}
          {/*    <img*/}
          {/*      src="https://pbs.twimg.com/profile_images/457131632260952064/00NbptD7_x96.jpeg"*/}
          {/*      alt="user"*/}
          {/*    />*/}
          {/*  </div>*/}
          {/*  <Hidden mdDown>*/}
          {/*    <div>*/}
          {/*      <div className="name">Victoria Shatun</div>*/}
          {/*      <div className="username">@VictoriaShizuo</div>*/}
          {/*    </div>*/}
          {/*  </Hidden>*/}
          {/*</button>*/}
        </footer>
      </div>
    </div>
  );
};
