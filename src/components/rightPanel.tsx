import React, { FC } from "react";
import { useHomeStyles } from "../pages/theme";
import { Tags } from "./Tags";
import { SearchField } from "./SearchField";
import { Users } from "./Users";

interface RightPanelProps {
  classes: ReturnType<typeof useHomeStyles>;
}

export const RightPanel: FC<RightPanelProps> = ({ classes }) => {
  return (
    <div className={classes.rightSide}>
      <SearchField />
      <Tags classes={classes} />
      <Users />
    </div>
  );
};
