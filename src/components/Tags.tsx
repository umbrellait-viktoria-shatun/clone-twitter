import React, { FC, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Paper, Typography } from "@material-ui/core";
import List from "@material-ui/core/List/List";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import Divider from "@material-ui/core/Divider/Divider";
import { selectTagsItems } from "../store/ducks/tags/selectors";
import { fetchTags } from "../store/ducks/tags/actionCreators";
import { useHomeStyles } from "../pages/theme";

interface TagsProps {
  classes: ReturnType<typeof useHomeStyles>;
}

export const Tags: FC<TagsProps> = ({ classes }: TagsProps) => {
  const dispatch = useDispatch();
  const tags = useSelector(selectTagsItems);

  useEffect(() => {
    dispatch(fetchTags());
  }, [dispatch]);

  return (
    <Paper className={classes.rightSideBlock}>
      <Paper className={classes.rightSideBlockHeader} variant="outlined">
        <b>Актуальные темы</b>
      </Paper>
      <List>
        {tags.map((obj) => (
          <React.Fragment key={obj._id}>
            <ListItem className={classes.rightSideBlockItem}>
              <Link to={`/explore?q=${obj.name}`}>
                <ListItemText
                  primary={obj.name}
                  secondary={
                    <Typography
                      component="span"
                      variant="body2"
                      color="textSecondary"
                    >
                      Твитов: {obj.count}
                    </Typography>
                  }
                />
              </Link>
            </ListItem>
            <Divider component="li" />
          </React.Fragment>
        ))}
      </List>
    </Paper>
  );
};
