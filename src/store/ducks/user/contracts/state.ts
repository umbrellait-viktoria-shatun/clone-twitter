import { LoadingStatus } from "../../../types";

export interface User {
  _id?: string;
  email: string;
  fullname: string;
  username: string;
  password: string;
  confirmHash: string;
  confirmed?: boolean;
  about?: string;
  website?: string;
  birthday?: string;
  createdAt?:Date;
}

export interface UserState {
  data: User | undefined;
  status: LoadingStatus;
}
